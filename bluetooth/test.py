"""
    Testing some code for comunication with an android device.
    Code sample from web.
"""

import bluetooth

bd_addr = "C0:EE:FB:F5:86:ED"
port = 1
sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
sock.connect(bd_addr.port)

data = ""
while 1:
    try:
        print("In try \n")
        data += sock.recv(1024)
        data_end = data.find('\n')
        if data_end != -1:
            rec = data[:data_end]
            print(data)
            data = data[data_end+1:]
    except KeyboardInterrupt:
        print("In except \n")
        break
sock.close()