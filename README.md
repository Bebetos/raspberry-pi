# RaspBerry Py #

This repository is dedicated for all Python Scripts for my Raspberry Pi 3.
It's permanent under development while I have free time, and all the test.py can contain sample by me or get from the web.
Feel free to contribute!


### Packages ###

* Bluetooth
* USB
-> B/G LED Monitor [Weather information]
* Telegram Bot [CODE ON REVIEW]
* Simple web server [For a little job]
-> Future implementations for other sensors.

### NOTE ###
All the test.py files can contain code samples from the web that i'm testing.
All the demo.py files can contain a little script for running my code.
