"""
    This is a sample file with constants for the connection of web services (eg. Apixu for weather informations).
    Remember to add your information and rename the file as config.py
"""

# API from apiux.com for free weather_api informations
APIUX_KEY = "8f7b2e999d5e4b0fbe6180307172308"
APIUX_CITY = "ARTENA"

# API from newsapi.org for free news
NEWSAPI_KEY = "f93a9af37b074d7b82cd7e6c1f44e028"

# Telegram Bot Key
TELEGRAM_KEY = "408583658:AAFAUu932O_rLzLPw1MJ5HqVxzdmHNva15M"