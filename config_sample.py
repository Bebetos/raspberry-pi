"""
    This is a sample file with constants for the connection of web services (eg. Apixu for weather informations).
    Remember to add your information and rename the file as config.py
"""

# API from apiux.com for free weather_api informations
APIUX_KEY=""
APIUX_CITY=""