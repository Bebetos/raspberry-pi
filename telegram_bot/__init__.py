import telepot
import time
import ipgetter
from config import api_key
from telepot.loop import MessageLoop


if __name__ == "__main__" :

    def handle(msg):

        chat_id = msg['chat']['id']
        command = msg['text']
        username = msg['from']['username']

        if username in api_key:


        if command != None:
            print('Got command: ' + command)

        if command.find(" ") != -1:
            parsedCommand = command[:command.find(" ")]
            parsedArg = command[command.find(" "):].lstrip()
        else:
            parsedCommand = command
            parsedArg = None

        if parsedCommand == "/ip":
            bot.sendMessage(chat_id, ipgetter.myip())


    bot = telepot.Bot(api_key.TELEGRAM_KEY)
    MessageLoop(bot, handle).run_as_thread()
    print('I am listening...')

    while 1:
        time.sleep(1)