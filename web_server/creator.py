"""
    This module is used for create a simple HTML page for a .txt file.
    This is based for a .txt file created by a private script that reads from an antenna connected via USB.
    TODO: Better style XD
          Page creator not linked to the format in txt file.
"""

class PageCreator:

    def getLinesFromFile(self):
        fd = open('results.txt', 'r')
        return fd.readlines()

    def createHTMLPage(self):

        fd = open('index.html', 'w')
        fd.write("<html>")
        fd.write("<head><title>Pagina risultati</title></head>")
        fd.write("<body>")
        fd.write("<table><tr><th>ID</th><th>Campo</th><th>Valore</th></tr>")

        lines = self.getLinesFromFile()

        for line in lines:
            values = line.split(";")
            fd.write("<tr>")

            for value in values:
                fd.write("<td>" + value + "</td>")

            fd.write("</tr>")

        fd.write("</table></body></html>")

    def getHTMLPage(self):

        fd = open("index.html", "r")
        return fd.read()

