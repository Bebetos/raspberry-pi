"""
    This module is dedicated to the monitor connected via USB
"""

import time
import comunication
import buffer
import news_api
import weather_api
import config


if __name__ == '__main__':

    myBuffer = buffer.Buffer()
    myComunication = comunication.Comunication()
    instance = news_api.News()

    myNews = instance.getDefaultNews()

    for elem in myNews:
        myBuffer.addMessage(elem, myBuffer.getMsgLengthR2(elem))

    myComunication.sendSpeed(myBuffer.getSpeed())

    while 1:

        # Writing something
        myComunication.sendMsgR2(config.api_key.APIUX_CITY, weather_api.get_weather())
        time.sleep(2)

        for i in range(0, myBuffer.getBufferSize()):
            myComunication.sendMsgR2("NEWS", myBuffer.getMsg(i))
            time.sleep(myBuffer.getSleepTime(i))

