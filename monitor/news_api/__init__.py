import json
import requests
import config
if __name__ == '__main__':

    url = 'https://newsapi.org/v2/top-headlines?sources=google-news-it'
    header = {'X-Api-Key': config.api_key.NEWSAPI_KEY}

    response = requests.get(url, headers=header)
    json_data = json.loads(response.text)

    if json_data["status"] == "ok":
        for elem in json_data["articles"]:
            print(elem["title"])