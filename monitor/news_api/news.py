import json

import requests

from config import api_key


class News:

    def __init__(self):
        self.CATEGORY = None
        self.QUERY = None
        self.LANGUAGES = None
        self.COUNTRIES = None
        self.SOURCES = None

    def setCategory(self, category):
        self.CATEGORY = category

    def setQuery(self, query):
        self.QUERY = query

    def setLanguages(self, languages):
        for elem in languages:
            self.LANGUAGES.append(elem)

    def setCountries(self, countries):
        for elem in countries:
            self.COUNTRIES.append(elem)

    def setSources(self, sources):
        for elem in sources:
            self.SOURCES.append(elem)

    def getDefaultNews(self):
        url = 'https://newsapi.org/v2/top-headlines?sources=google-news'
        header = {'X-Api-Key': api_key.NEWSAPI_KEY}

        response = requests.get(url, headers=header)
        json_data = json.loads(response.text)

        if json_data["status"] == "ok":
            news = []
            for elem in json_data["articles"]:
                print(elem["title"])
                news.append(elem["title"].encode('utf-8'))

            return news
        else:
            return None


if __name__ == "__main__":
    print()
    