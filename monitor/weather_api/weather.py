"""
    This module will contain all the information about the weather information gathered from apiux.

    Work in progress!
    TODO: Get more weather informations.
"""

from apixu.client import ApixuClient
from config import api_key


def get_weather():
    client = ApixuClient(api_key.APIUX_KEY)
    current = client.getCurrentWeather(api_key.APIUX_CITY)
    return current['current']['temp_c']  # show temprature in celsius


print(get_weather())
