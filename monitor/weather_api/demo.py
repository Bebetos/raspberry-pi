"""
    This script was used for testing my solution for 2 rows messages
"""

import serial
import time


def getMsgLength(msg):

    val4 = ['I', '1', '[', ']', '(', ')', ',', '.', ':', '|' , '!', '\'']
    val6 = ['T', 'Y', 'f', 't', '{', '}', '-', '+']
    msgLength = 0
    for i in range(0,len(msg)):
        #msg[i] == I or 1 or ' 
        if(msg[i] in val4):
            msgLength += 4
        elif(msg[i] in val6):
            msgLength += 6
        else:
            msgLength += 7
    
    return msgLength


def getL2MsgLength(msg):
    
    val1 = ['|', '!']
    val2 = ['.', ',', '\'','(', ')']
    val3 = ['I', 'i', 'l','1', '[', ']', '{', '}']
    val4 = ['F', 'L', 'c', 'j', 'k', 't', 'y', '-', '_', '=']
    
    msgLength = 0
    for i in range(0,len(msg)):
        #msg[i] == I or 1 or ' 
        if(msg[i] in val1):
            msgLength += 2
        elif(msg[i] in val2):
            msgLength += 3
        elif(msg[i] in val3):
            msgLength += 4
        elif(msg[i] in val4):
            msgLength += 5
        elif(msg[i] == ' '):
            msgLength += 7
        else:
            msgLength += 6
    
    return msgLength


def getTime(msg, speed):

    msgLength = getL2MsgLength(msg)
    
    # Speed A-M
    if(speed < 7):
        time = 0.0024 * speed + 0.0123
        total_time = time * msgLength
        return total_time
    # Speed N-Z
    else:
        # Tempo "base"
        time = 0.0113 * speed - 0.0546
        total_time = time * msgLength
        return total_time 

def getSpeedMsg(speedIndex):
        strIndex = chr(speedIndex + 64)
        print('Speed: ' + strIndex)
        speedStr = '<ID01><SPD' + strIndex + '><E>'
        print(speedStr)
        return speedStr

if __name__ == '__main__':
    ser = serial.Serial(
        port='/dev/ttyUSB0',
        baudrate=9600,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=1
    )        
    
    
    for speed in range(1,27):
        
        ser.write(getSpeedMsg(speed))
        ser.write('<ID01><PA>--------<E>')
        time.sleep(3)
        
        msg = 'Ciao a tutti! Algoritmo funzionante! Evviva!!'
        toSend = '<ID01><L1>Welcome<L2>' + msg  + '<E>'
        ser.write(toSend)
        time.sleep(getTime(msg, speed))
        
        msgFinal = 'Non funziona un cazzo di niente! Dannazione!!'
        toSend = '<ID01><L1>Welcome<L2>' + msgFinal  + '<E>'
        ser.write(toSend)
        time.sleep(getTime(msgFinal, speed))

