"""
    This script containts all the functions to call and write directly on the monitor.
    Remember to enable the USB read/write permissions.
"""

import serial


class Comunication:

    def __init__(self):
        self.ser = serial.Serial(
            port='/dev/ttyUSB0',
            baudrate=9600,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=1)



    def sendSpeed(self, speed):
        strIndex = chr(speed + 64)
        self.ser.write('<ID01><SPD' + strIndex + '><E>')


    def turnOff(self):
        self.ser.write("<ID01><PA><E>")


    def sendMsgR1(self, msg):
        self.ser.write("<ID01><PA>" + msg + "<E>")


    def sendMsgR2(self, line1, line2):
        self.ser.write('<ID01><L1>' + str(line1) + '<L2>' + str(line2) + ' C <E>')
