
"""
    This module containts all the utility functions about the B/G LED message board.
    Functions for length calculation and time calculation.
    Suffix R2 means that the functions are for the two rows mode.
"""

class Buffer:

    def __init__(self):
        self.message = []
        self.length = []
        self.sleepTime = []
        self.speed = 7

    def addMessage(self, msg, length):
        self.message.append(msg)
        self.length.append(length)
        self.sleepTime.append(self.getTime(length))

    def getMsg(self, i):
        return self.message[i]

    def getSleepTime(self, i):
        return self.sleepTime[i]

    def getBufferSize(self):
        return len(self.message)

    def getSpeed(self):
        return self.speed

    def setSpeed(self, s):
        self.speed = s

    def getMsgLength(self, msg):
        """
        This function is used for calculate the length of a single row message.
    
        :param msg: A string with the message
        :return: An integer with the length (in pixel) of the message
    
        """
        val4 = ['I', '1', '[', ']', '(', ')', ',', '.', ':', '|', '!', '\'']
        val6 = ['T', 'Y', 'f', 't', '{', '}', '-', '+']
        msgLength = 0
        for i in range(0, len(msg)):
            # msg[i] == I or 1 or '
            if (msg[i] in val4):
                msgLength += 4
            elif (msg[i] in val6):
                msgLength += 6
            else:
                msgLength += 7

        return msgLength

    def getMsgLengthR2(self, msg):
        """
        This function is used for calculate the length of a two rows message.
        REMEMBER: first line is static!
    
        :param msg: A string with the message
        :return: An integer with the length (in pixel) of the message
    
        """
        val1 = ['|', '!']
        val2 = ['.', ',', '\'', '(', ')']
        val3 = ['I', 'i', 'l', '1', '[', ']', '{', '}']
        val4 = ['F', 'L', 'c', 'j', 'k', 't', 'y', '-', '_', '=']

        msgLength = 0
        for i in range(0, len(msg)):
            # msg[i] == I or 1 or '
            if (msg[i] in val1):
                msgLength += 2
            elif (msg[i] in val2):
                msgLength += 3
            elif (msg[i] in val3):
                msgLength += 4
            elif (msg[i] in val4):
                msgLength += 5
            elif (msg[i] == ' '):
                msgLength += 7
            else:
                msgLength += 6

        return msgLength

    def getTime(self, msgLength):
        """
        This function is used for calculate the sleeping time in case of a single row message.
    
        :param msg: The string with the message
        :param speed: An integer with the speed index (from 1 to 27)
        :return: The sleeping time of the system
    
        TODO: need to check this values!
        """

        # Speed A-M
        if self.speed < 7:
            time = 0.0024 * self.speed + 0.0123
            total_time = time * msgLength
            return total_time
        # Speed N-Z
        else:
            # Tempo "base"
            time = 0.0113 * self.speed - 0.0546
            total_time = time * msgLength
            return total_time
